# app.py
from flask import Flask, request, jsonify
from shapely.geometry import shape
import json 
import geopandas

app = Flask(__name__)


def eprint(*args, **kwargs):
        print(*args, file=sys.stderr, **kwargs)


polygons = [
]
polygonos = [
]

def _find_next_id():
    print(polygons)
    if len(polygons) > 0:
        return max(polygon["id"] for polygon in polygons) + 1
    else:
        return 1
    
@app.get("/centroids")
def get_countries():
    return jsonify(polygons)

@app.post("/centroids")
def add_polygon():
    if request.is_json:
        polygon = json.loads(request.get_json())
        polyCoords = polygon["features"][0]["geometry"]["coordinates"]
        polyShape = shape({
            "type": "Polygon",
            "coordinates":polyCoords
            })
        polygon["centroidx"] = polyShape.centroid.x
        polygon["centroidy"] = polyShape.centroid.y
        polygon["id"] = _find_next_id()
        polygon["name"] = polygon["features"][0]["properties"]["INSEE_COM"]
        
        #remove unwanted keys
        polygon.pop("features")
        polygon.pop("type")
        
        polygons.append(polygon)
        
        return polygons, 201
    return {"error": "Request must be JSON"}, 415


####

@app.get("/test")
def get_countries():
    return jsonify(polygonos)

# Create another service, capable of returning all the dataset
@app.get("/test")
def add_polygons_test():
    # if request.is_json:
        #open geospatial data
        facDf =  geopandas.read_file('./geospatialRestApi/data/ign_coms_dep31_2022.shp')

        polygono = facDf.to_json()
        polygonos.append(polygono)
        return polygonos, 201

    # return {"error": "Request must be JSON"}, 415


# @app.get("/test")
# def get_countries_test():
#     return 'HELLO YOU'

